﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChooser : MonoBehaviour
{
    static LevelChooser instance;

    List<string> scenes = new List<string>();

    private OVRInput.Axis1D grip = OVRInput.Axis1D.PrimaryHandTrigger;

    //void Awake()
    //{
    //    if (instance != null)
    //    {
    //        Destroy(gameObject);
    //    }
    //    else
    //    {
    //        instance = this;
    //        DontDestroyOnLoad(gameObject);
    //    }
    //    //scenes.Add("Drone");
    //    //scenes.Add("Fairy");
    //    //scenes.Add("Hummingbird");
    //}

    void Update()
    {
        if (OVRInput.Get(grip) == 1f)
        {
            //if (GameObject.Find("MovementManager") != null)
            //{
            //    MovementManager movementManager = GameObject.Find("MovementManager").GetComponent<MovementManager>();
            //    if (movementManager.IsDone())
            //    {
            //        int index = Random.Range(0, scenes.Count);
            //        StartCoroutine(LoadAsyncScene(scenes[index]));
            //        scenes.RemoveAt(index);
            //    }
            //}
            //else
            //{
            //int index = Random.Range(0, scenes.Count);
            //StartCoroutine(LoadAsyncScene(scenes[index]));
            //scenes.RemoveAt(index);
            //}
            SceneManager.LoadScene(1);
        }
    }

    IEnumerator LoadAsyncScene(string sceneName)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);

        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
