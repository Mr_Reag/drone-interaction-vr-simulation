﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementManager : MonoBehaviour
{

    public Transform droneRoot;
    public Transform targetPosition;
    public Animator faceAnimator;
    public string animationName;
    [SerializeField]
    private float approachTime = 10f;


    public List<GameObject> propellers;


    private OVRInput.Axis1D trigger = OVRInput.Axis1D.PrimaryIndexTrigger;

    public void Start()
    {
        StartCoroutine(Movement());
    }

    private void Update()
    {
        if (propellers.Count != 0)
        {
            foreach (GameObject propeller in propellers) { propeller.transform.Rotate(0, 10, 0); }
        }   
    }

    private IEnumerator Movement()
    {
        
        var initPos = droneRoot.position;
        var endPos = targetPosition.position;

        yield return new WaitForSecondsRealtime(1f);

        //Debug.Log("Staring Movement");

        float deltat = 0;
        while(deltat < approachTime)
        {
            yield return new WaitForEndOfFrame();
            deltat += Time.deltaTime;
            //Debug.Log(deltat);
            droneRoot.position = Vector3.Lerp(initPos, endPos, deltat / approachTime);
        }
        yield return new WaitForEndOfFrame();
        //Debug.Log("Approach Finished, running animation");
        faceAnimator.SetTrigger(animationName);
        yield return new WaitForSecondsRealtime(.4f);
        yield return new WaitForSecondsRealtime(10f);
        //End of experiment stuff goes here
        yield return null;
    }

}
