﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer)),ExecuteAlways]
public class AnimateLineRenderer : MonoBehaviour
{
    [SerializeField]
    private Transform[] anchorPoints;
    private LineRenderer rend;


    void Update()
    {
        Debug.Log("Frame");
        if (!IsValidComponent()) return;
        var pos = new Vector3[anchorPoints.Length];

        for (int i = 0; i < anchorPoints.Length; i++) pos[i] = anchorPoints[i].position;
        rend.SetVertexCount(pos.Length);
        rend.SetPositions(pos);
        
    }


    private bool IsValidComponent()
    {
        if (!rend) rend = GetComponent<LineRenderer>();
        return NoNull() && anchorPoints.Length > 0;
    }

    private bool NoNull()
    {
        foreach(var t in anchorPoints)
        {
            if (!t) return false;
        }
        return true;
    }
}
