﻿using UnityEngine;
using System.Collections;

public class HummingbirdAICharacterScript : MonoBehaviour {
    public Animator hummingbirdAnimator;
    public float animSpeed = 5f;
    Rigidbody hummingbirdRigid;
    public bool isFlying = false;
    public float forwardSpeed = 3f;
    public float hopSpeed = 2f;
    public float rotateSpeed = .2f;
    public float upSpeed = 0f;
    public GameObject flower;
    public int nectarMode = 0;
    float nectarStartTime = 0f;
    float nectarEndTime = 0f;

    GameObject flowerEmpty;
    GameObject nectarEmpty;
    
    public HummingbirdFlowers flowers;

    void Start()
    {
        hummingbirdAnimator = GetComponent<Animator>();
        hummingbirdAnimator.speed = animSpeed;
        hummingbirdRigid = GetComponent<Rigidbody>();
        ChooseRandomFlower();
        Soar();
    }

    void FixedUpdate()
    {
        if (isFlying)
        {
            switch (nectarMode)
            {
                case 0:
                        GoFlower();
                    break;
                case 1:
                        Positioning();
                    break;
                case 2:
                    nectarStartTime+=Time.deltaTime;
                    if (nectarStartTime > 3f)
                    {
                        nectarMode = 3;
                        nectarEndTime = 0f;
                        NectarEnd();
                        forwardSpeed = -1f;
                        upSpeed = 0f;
                        rotateSpeed = 0f;

                    }
                    break;
                case 3:
                    Move();
                    nectarEndTime+= Time.deltaTime;
                    if (nectarEndTime>1f)
                    {
                        nectarMode = 4;
                    }
                    break;
                case 4:
                    ChooseRandomFlower();
                    nectarMode = 0;
                    break;

                default:

                    break;

            }
       }
    }

    void ChooseRandomFlower()
    {
        flower = flowers.flower[Random.Range(0,flowers.flower.Length)];
        flowerEmpty = flower.GetComponent<HummingbirdFlower>().flowerPositionEmpty;
        nectarEmpty= flower.GetComponent<HummingbirdFlower>().nectarPositionEmpty;

    }

    void GoFlower()
    {
        Vector3 flowerRelPos = flowerEmpty.transform.position - transform.position;
        
        rotateSpeed = Vector3.Dot(transform.right, Vector3.Normalize(flowerRelPos)) ;
        forwardSpeed = 1.4f;

        upSpeed = Vector3.Dot(transform.up, flowerRelPos);

        Move();

        if (Vector3.SqrMagnitude(flowerRelPos)<.03f)
        {
            nectarMode = 1;
        }
    }



    void Positioning()
    {

        transform.position = Vector3.Lerp(transform.position, nectarEmpty.transform.position, Time.deltaTime);
        transform.rotation = Quaternion.Lerp(transform.rotation, nectarEmpty.transform.rotation, 4f*Time.deltaTime);

        forwardSpeed=Mathf.Lerp(forwardSpeed,-1f,Time.deltaTime);
        hummingbirdAnimator.SetFloat("Forward", forwardSpeed);

        if (Vector3.SqrMagnitude(nectarEmpty.transform.position - transform.position) < .001f && Quaternion.Angle(transform.rotation, nectarEmpty.transform.rotation)<0.5)
        {
            nectarMode = 2;
            nectarStartTime = 0f;
            NectarStart();
        }

    }

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.GetComponent<HummingbirdAICharacterScript>() != null)
        {
            hummingbirdAnimator.SetBool("IsNectar", false);
            nectarMode = 0;
            ChooseRandomFlower();
        }

    }

    public void AnimSpeedSet(float speed)
    {
        animSpeed = speed;
        hummingbirdAnimator.speed = animSpeed;
    }

    public void Landing()
    {
        hummingbirdAnimator.SetTrigger("Landing");
        hummingbirdRigid.useGravity = true;
        isFlying = false;
    }

    public void Soar()
    {
        hummingbirdAnimator.SetTrigger("Soar");
        hummingbirdRigid.useGravity = false;
        isFlying = true;
    }

    public void HopForward()
    {
        hummingbirdAnimator.SetTrigger("HopForward");
        hummingbirdRigid.AddForce((transform.forward + transform.up) * hopSpeed, ForceMode.Impulse);
    }

    public void HopRight()
    {
        hummingbirdAnimator.SetTrigger("HopRight");
        hummingbirdRigid.AddForce((transform.forward + transform.up) * hopSpeed, ForceMode.Impulse);
        hummingbirdRigid.AddTorque(transform.up * rotateSpeed, ForceMode.Impulse);
    }

    public void HopLeft()
    {
        hummingbirdAnimator.SetTrigger("HopLeft");

        hummingbirdRigid.AddForce((transform.forward + transform.up) * hopSpeed, ForceMode.Impulse);
        hummingbirdRigid.AddTorque(-transform.up * rotateSpeed, ForceMode.Impulse);
    }


    public void Attack()
    {
        hummingbirdAnimator.SetTrigger("Attack");
    }

    public void LookAround()
    {
        hummingbirdAnimator.SetTrigger("LookAround");
    }

    public void SitDown()
    {
        hummingbirdAnimator.SetTrigger("SitDown");
    }

    public void StandUp()
    {
        hummingbirdAnimator.SetTrigger("StandUp");
    }

    public void StandFlap()
    {
        hummingbirdAnimator.SetTrigger("StandFlap");
    }

    public void NectarStart()
    {
        hummingbirdAnimator.SetBool("IsNectar", true);
    }


    public void NectarEnd()
    {
        hummingbirdAnimator.SetBool("IsNectar", false);
    }



    public void Hit()
    {
        hummingbirdAnimator.SetTrigger("Hit");
    }

    public void Move()
    {
        if (isFlying)
        {
            hummingbirdRigid.AddForce((transform.forward * forwardSpeed*0.1f+ transform.up * upSpeed*0.06f) );


            hummingbirdRigid.AddTorque(transform.up * rotateSpeed*0.004f);
            hummingbirdAnimator.SetFloat("UpDown", upSpeed);
            hummingbirdAnimator.SetFloat("Forward", forwardSpeed);
            hummingbirdAnimator.SetFloat("Turn", rotateSpeed);

        }
        else
        {
            hummingbirdAnimator.SetFloat("Forward", forwardSpeed);
            hummingbirdAnimator.SetFloat("Turn", rotateSpeed);
        }
    }
}
